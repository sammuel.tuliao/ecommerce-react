import React, { useContext } from 'react';
import { CartContext } from './Cart';
import { Link } from 'react-router-dom';
import "../styles/Details.css";
const CartDetails = () => {
  const { cart, dispatch } = useContext(CartContext);

  const handleRemove = (item) => {
    dispatch({ type: 'removeItem', payload: item });
 
  };

 
  const cartItems = cart?.map((item) => (
    <div className="details-card">
    <div key={item.id}>
      
        <div className="details-image">
          <img src={item.image} alt={item.title} />
        </div>
        <div className="details-info">
          <h2>{item.title}</h2>
          <p className="details-price">${item.price}</p>
          <button className="btn details-button" onClick={() => handleRemove(item)}>Remove</button>
        </div>
      </div>
    </div>
  ));

  return (
    
    <div >
      
      <Link to={'/'}>
                <button>Home</button></Link>
      {cart.length > 0 ? (
        <>
        
          <h2>Cart</h2>
          {cartItems}
          
        </>
      ) : (
        <h2>Your cart is empty</h2>
      )}
    </div>
  );
};

export default CartDetails;