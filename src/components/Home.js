import React, { useState, useEffect, useContext } from 'react';
import { Link } from "react-router-dom";
import { CartContext } from './Cart';
import "../styles/Item.css";
function Home() {
    const [products, setProducts] = useState([]);
    const [searchProduct, setSearchProduct] = useState('');
    const { dispatch } = useContext(CartContext);

    useEffect(() => {
        fetch('https://fakestoreapi.com/products')
            .then(res=>res.json())
            .then(data => setProducts(data))
            .catch(error => console.error(error));
    }, []);

    const handleAddToCart = (product) => {
        dispatch({ type: 'addItem', payload: product });
      };

    const filteredProducts = products.filter(product => {
        const title = product.title.toLowerCase();
        return title.includes(searchProduct.toLowerCase());
      });

    if(!products){
        return <div>Loading...</div>
    };
  
    return (
      <div>
        <Link to={'/cart'}><button>View Cart</button></Link>
        <input type="text" placeholder="Search by title..." value={searchProduct} 
        onChange={e => setSearchProduct(e.target.value)} />
        <h1>Products</h1>
        {filteredProducts.map(product => (
          
          // <li key={product.id}>
          //     <p>{product.title}</p>
          //     <p>{product.price}</p>
          //     <Link to={`/products/${product.id}`}>
          //       <button>View Details</button></Link>
          //       <button onClick={() => handleAddToCart(product)}>Add to Cart</button>
          // </li>
          
          <div className="product-card">
      <div className="product-image">
        <img src={product.image} alt={product.title} />
      </div>
      <div className="product-info">
        <h2>{product.title}</h2>
        <p className="product-price">${product.price}</p>
        <Link to={`/products/${product.id}`}>
          <button className="btn product-button" >View Details</button></Link>
          <button className="btn product-button"  onClick={() => handleAddToCart(product)} >Add to cart</button>

      </div>
    </div>
          

        ))}
      
      
      </div>
    );
  }
  
  export default Home;