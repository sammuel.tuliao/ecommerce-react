import React, { useState, useEffect, useContext } from 'react';
import { useParams, Link } from 'react-router-dom';
import { CartContext } from './Cart';
import "../styles/Cart.css";
function ProductDetails() {
  // Use the match prop to get the ID of the selected product
  const [product, setProduct] = useState([]);
  const { productId } = useParams();
  const { dispatch } = useContext(CartContext);
  useEffect(() => {
    fetch(`https://fakestoreapi.com/products/${productId}`)
        .then(res=>res.json())
        .then(data => setProduct(data))
        .catch(error => console.error(error));
}, [productId]);

const handleAddToCart = (product) => {
  console.log(product)
  dispatch({ type: 'addItem', payload: product });
};

  if (!product) {
    return <div>Loading...</div>;
  }
  return (
    <div><Link to={'/'}>
    <button>Home</button></Link>    <div className="cart-card">
      
      <div className="cart-info">
      <div className="cart-image">
        <img src={product.image} alt={product.title} />
      </div>
        <h2>{product.title}</h2>
        <h2>{product.description}</h2>
        <p className="cart-price">${product.price}</p>
        
        <button onClick={() => handleAddToCart(product)} className="btn product-button">Add to Cart</button>
       
      </div>
    </div>
    </div>
   
  );

}


export default ProductDetails;