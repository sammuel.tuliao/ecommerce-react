import React, { createContext, useReducer } from 'react';

export const CartContext = createContext();

const cartReducer = (state, action) => {
    
  switch (action.type) {
    case 'addItem':
      const item = action.payload;
      const existingItem = state.find((i) => i.id === item.id);

      if (existingItem) {
        return state.map((i) =>
          i.id === item.id ? { ...i } : i
        );
      } else {
        return [...state, item];
      }
      case 'removeItem':
        return state.filter((i) => i.id !== action.payload.id);
      

    default:
      return state;
  }
};

export const CartProvider = ({ children }) => {
  const [cart, dispatch] = useReducer(cartReducer, []);

  return (
    <CartContext.Provider value={{ cart, dispatch }}>
      {children}
    </CartContext.Provider>
  );
};